﻿namespace Neural_Search
{
    partial class main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSearch = new System.Windows.Forms.Button();
            this.listOfImages = new System.Windows.Forms.ListView();
            this.txtSearchBox = new System.Windows.Forms.TextBox();
            this.grbStats = new System.Windows.Forms.GroupBox();
            this.lblCircles = new System.Windows.Forms.Label();
            this.lblQuadrilaterals = new System.Windows.Forms.Label();
            this.lblTriangles = new System.Windows.Forms.Label();
            this.lblLoaded = new System.Windows.Forms.Label();
            this.btnAddImage = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.grbInfo = new System.Windows.Forms.GroupBox();
            this.lblSize = new System.Windows.Forms.Label();
            this.lblExtension = new System.Windows.Forms.Label();
            this.lblHWstat = new System.Windows.Forms.Label();
            this.lblNamePic = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.grbSearchString = new System.Windows.Forms.GroupBox();
            this.grbSearchOperations = new System.Windows.Forms.GroupBox();
            this.grbSearch = new System.Windows.Forms.GroupBox();
            this.grbResult = new System.Windows.Forms.GroupBox();
            this.grbStats.SuspendLayout();
            this.grbInfo.SuspendLayout();
            this.grbSearchString.SuspendLayout();
            this.grbSearchOperations.SuspendLayout();
            this.grbSearch.SuspendLayout();
            this.grbResult.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(6, 19);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(70, 25);
            this.btnSearch.TabIndex = 0;
            this.btnSearch.Text = "Поиск";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // listOfImages
            // 
            this.listOfImages.Location = new System.Drawing.Point(6, 19);
            this.listOfImages.Name = "listOfImages";
            this.listOfImages.Size = new System.Drawing.Size(455, 470);
            this.listOfImages.TabIndex = 1;
            this.listOfImages.UseCompatibleStateImageBehavior = false;
            this.listOfImages.ItemActivate += new System.EventHandler(this.listOfImages_ItemActivate);
            this.listOfImages.Click += new System.EventHandler(this.listOfImages_Click);
            // 
            // txtSearchBox
            // 
            this.txtSearchBox.Location = new System.Drawing.Point(6, 19);
            this.txtSearchBox.Name = "txtSearchBox";
            this.txtSearchBox.Size = new System.Drawing.Size(300, 20);
            this.txtSearchBox.TabIndex = 2;
            // 
            // grbStats
            // 
            this.grbStats.Controls.Add(this.lblCircles);
            this.grbStats.Controls.Add(this.lblQuadrilaterals);
            this.grbStats.Controls.Add(this.lblTriangles);
            this.grbStats.Controls.Add(this.lblLoaded);
            this.grbStats.Location = new System.Drawing.Point(467, 101);
            this.grbStats.Name = "grbStats";
            this.grbStats.Size = new System.Drawing.Size(200, 75);
            this.grbStats.TabIndex = 3;
            this.grbStats.TabStop = false;
            this.grbStats.Text = "Статистика";
            // 
            // lblCircles
            // 
            this.lblCircles.AutoSize = true;
            this.lblCircles.Location = new System.Drawing.Point(6, 29);
            this.lblCircles.Name = "lblCircles";
            this.lblCircles.Size = new System.Drawing.Size(48, 13);
            this.lblCircles.TabIndex = 3;
            this.lblCircles.Text = "Кругов: ";
            // 
            // lblQuadrilaterals
            // 
            this.lblQuadrilaterals.AutoSize = true;
            this.lblQuadrilaterals.Location = new System.Drawing.Point(6, 55);
            this.lblQuadrilaterals.Name = "lblQuadrilaterals";
            this.lblQuadrilaterals.Size = new System.Drawing.Size(121, 13);
            this.lblQuadrilaterals.TabIndex = 2;
            this.lblQuadrilaterals.Text = "Четырехугольников: n";
            // 
            // lblTriangles
            // 
            this.lblTriangles.AutoSize = true;
            this.lblTriangles.Location = new System.Drawing.Point(6, 42);
            this.lblTriangles.Name = "lblTriangles";
            this.lblTriangles.Size = new System.Drawing.Size(96, 13);
            this.lblTriangles.TabIndex = 1;
            this.lblTriangles.Text = "Треугольников: n";
            // 
            // lblLoaded
            // 
            this.lblLoaded.AutoSize = true;
            this.lblLoaded.Location = new System.Drawing.Point(6, 16);
            this.lblLoaded.Name = "lblLoaded";
            this.lblLoaded.Size = new System.Drawing.Size(151, 13);
            this.lblLoaded.TabIndex = 0;
            this.lblLoaded.Text = "Загруженно изображений: n";
            // 
            // btnAddImage
            // 
            this.btnAddImage.Location = new System.Drawing.Point(185, 19);
            this.btnAddImage.Name = "btnAddImage";
            this.btnAddImage.Size = new System.Drawing.Size(151, 25);
            this.btnAddImage.TabIndex = 4;
            this.btnAddImage.Text = "Добавить изображение";
            this.btnAddImage.UseVisualStyleBackColor = true;
            this.btnAddImage.Click += new System.EventHandler(this.btnAddImage_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.Filter = "Изображения |*.bmp;*.png;*.jpg";
            // 
            // grbInfo
            // 
            this.grbInfo.Controls.Add(this.lblSize);
            this.grbInfo.Controls.Add(this.lblExtension);
            this.grbInfo.Controls.Add(this.lblHWstat);
            this.grbInfo.Controls.Add(this.lblNamePic);
            this.grbInfo.Location = new System.Drawing.Point(467, 19);
            this.grbInfo.Name = "grbInfo";
            this.grbInfo.Size = new System.Drawing.Size(200, 76);
            this.grbInfo.TabIndex = 5;
            this.grbInfo.TabStop = false;
            this.grbInfo.Text = "Информация";
            // 
            // lblSize
            // 
            this.lblSize.AutoSize = true;
            this.lblSize.Location = new System.Drawing.Point(6, 55);
            this.lblSize.Name = "lblSize";
            this.lblSize.Size = new System.Drawing.Size(60, 13);
            this.lblSize.TabIndex = 3;
            this.lblSize.Text = "Вес: пусто";
            // 
            // lblExtension
            // 
            this.lblExtension.AutoSize = true;
            this.lblExtension.Location = new System.Drawing.Point(6, 29);
            this.lblExtension.Name = "lblExtension";
            this.lblExtension.Size = new System.Drawing.Size(104, 13);
            this.lblExtension.TabIndex = 2;
            this.lblExtension.Text = "Расширение: пусто";
            // 
            // lblHWstat
            // 
            this.lblHWstat.AutoSize = true;
            this.lblHWstat.Location = new System.Drawing.Point(6, 42);
            this.lblHWstat.Name = "lblHWstat";
            this.lblHWstat.Size = new System.Drawing.Size(88, 13);
            this.lblHWstat.TabIndex = 1;
            this.lblHWstat.Text = "Размеры: пусто";
            // 
            // lblNamePic
            // 
            this.lblNamePic.AutoSize = true;
            this.lblNamePic.Location = new System.Drawing.Point(6, 16);
            this.lblNamePic.Name = "lblNamePic";
            this.lblNamePic.Size = new System.Drawing.Size(91, 13);
            this.lblNamePic.TabIndex = 0;
            this.lblNamePic.Text = "Название: пусто";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(82, 19);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(97, 25);
            this.btnClear.TabIndex = 6;
            this.btnClear.Text = "Очистить поиск";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // grbSearchString
            // 
            this.grbSearchString.Controls.Add(this.txtSearchBox);
            this.grbSearchString.Location = new System.Drawing.Point(6, 13);
            this.grbSearchString.Name = "grbSearchString";
            this.grbSearchString.Size = new System.Drawing.Size(312, 50);
            this.grbSearchString.TabIndex = 7;
            this.grbSearchString.TabStop = false;
            this.grbSearchString.Text = "Строка поиска";
            // 
            // grbSearchOperations
            // 
            this.grbSearchOperations.Controls.Add(this.btnSearch);
            this.grbSearchOperations.Controls.Add(this.btnAddImage);
            this.grbSearchOperations.Controls.Add(this.btnClear);
            this.grbSearchOperations.Location = new System.Drawing.Point(324, 13);
            this.grbSearchOperations.Name = "grbSearchOperations";
            this.grbSearchOperations.Size = new System.Drawing.Size(343, 50);
            this.grbSearchOperations.TabIndex = 8;
            this.grbSearchOperations.TabStop = false;
            this.grbSearchOperations.Text = "Операции";
            // 
            // grbSearch
            // 
            this.grbSearch.Controls.Add(this.grbSearchString);
            this.grbSearch.Controls.Add(this.grbSearchOperations);
            this.grbSearch.Location = new System.Drawing.Point(6, 7);
            this.grbSearch.Name = "grbSearch";
            this.grbSearch.Size = new System.Drawing.Size(673, 68);
            this.grbSearch.TabIndex = 9;
            this.grbSearch.TabStop = false;
            this.grbSearch.Text = "Поиск";
            // 
            // grbResult
            // 
            this.grbResult.Controls.Add(this.listOfImages);
            this.grbResult.Controls.Add(this.grbInfo);
            this.grbResult.Controls.Add(this.grbStats);
            this.grbResult.Location = new System.Drawing.Point(6, 81);
            this.grbResult.Name = "grbResult";
            this.grbResult.Size = new System.Drawing.Size(673, 495);
            this.grbResult.TabIndex = 10;
            this.grbResult.TabStop = false;
            this.grbResult.Text = "Результат";
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 579);
            this.Controls.Add(this.grbResult);
            this.Controls.Add(this.grbSearch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "main";
            this.Text = "Поиск изображений по критериям";
            this.grbStats.ResumeLayout(false);
            this.grbStats.PerformLayout();
            this.grbInfo.ResumeLayout(false);
            this.grbInfo.PerformLayout();
            this.grbSearchString.ResumeLayout(false);
            this.grbSearchString.PerformLayout();
            this.grbSearchOperations.ResumeLayout(false);
            this.grbSearch.ResumeLayout(false);
            this.grbResult.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.ListView listOfImages;
        private System.Windows.Forms.TextBox txtSearchBox;
        private System.Windows.Forms.GroupBox grbStats;
        private System.Windows.Forms.Label lblLoaded;
        private System.Windows.Forms.Label lblTriangles;
        private System.Windows.Forms.Label lblQuadrilaterals;
        private System.Windows.Forms.Label lblCircles;
        private System.Windows.Forms.Button btnAddImage;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.GroupBox grbInfo;
        private System.Windows.Forms.Label lblExtension;
        private System.Windows.Forms.Label lblHWstat;
        private System.Windows.Forms.Label lblNamePic;
        private System.Windows.Forms.Label lblSize;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.GroupBox grbSearchString;
        private System.Windows.Forms.GroupBox grbSearchOperations;
        private System.Windows.Forms.GroupBox grbSearch;
        private System.Windows.Forms.GroupBox grbResult;
    }
}

