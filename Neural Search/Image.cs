﻿using System.Collections.Generic;

namespace Neural_Search
{
    class Image
    {
        public string name { get; }
        public string extension { get; }
        private List<string> tags = new List<string>();
        public int height { get; }
        public int width { get; }
        public long size { get; }
        public List<string> getTags() { return tags; }
        public Image(string name, string extension, List<string> tags, int height, int width, long size)
        {
            this.name = name;
            this.extension = extension;
            this.tags = tags;
            this.height = height;
            this.width = width;
            this.size = size;
        }
    }
}
