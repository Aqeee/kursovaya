﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Windows.Forms;

using Accord;
using Accord.Controls;
using Accord.Imaging;
using Accord.Imaging.Filters;
using Accord.Math.Geometry;


namespace Neural_Search
{
    public partial class main : Form
    {
        private List<Image> imagesList = new List<Image>();
        private int imagesCount;
        private int Quadrilateral;
        private int Circle;
        private int Trapezoid;
        private int Parallelogram;
        private int Rectangle;
        private int Rhombus;
        private int Square;
        private int Triangle;
        private int EquilateralTriangle;
        private int IsoscelesTriangle;
        private int RectangledTriangle;
        private int RectangledIsoscelesTriangle;
        Dictionary<string, Bitmap> displayImages;

        public main()
        {
            InitializeComponent();
        }

        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);
                listOfImages.Clear();
                imagesList.Clear();

                imagesCount = 0;
                Quadrilateral = 0;
                Circle = 0;
                Trapezoid = 0;
                Parallelogram = 0;
                Rectangle = 0;
                Rhombus = 0;
                Square = 0;
                Triangle = 0;
                EquilateralTriangle = 0;
                IsoscelesTriangle = 0;
                RectangledTriangle = 0;
                RectangledIsoscelesTriangle = 0;

                // путь до папки директории
                var path = new DirectoryInfo(Path.Combine(Application.StartupPath, "Resources"));

                displayImages = new Dictionary<string, Bitmap>();

                ImageList imageList = new ImageList();
                imageList.ImageSize = new Size(64, 64);
                imageList.ColorDepth = ColorDepth.Depth32Bit;
                listOfImages.LargeImageList = imageList;

                FileInfo[] files = GetFilesByExtensions(path, ".jpg", ".png", ".bmp").ToArray();
                imagesCount = files.Count();

                for (int i = 0; i < files.Length; i++)
                {
                    FileInfo file = files[i];
                    Bitmap image = (Bitmap)Bitmap.FromFile(file.FullName);
                    string shortName = file.Name;
                    string imageKey = file.FullName;

                    imageList.Images.Add(imageKey, image);
                    displayImages.Add(imageKey, image);

                    ListViewItem item = new ListViewItem();
                    item.ImageKey = imageKey;
                    item.Name = shortName;
                    item.Text = shortName;

                    //Тут должно быть создание объекта и анализ его качеств и добавление его в контейнер объектов
                    List<string> tags = new List<string>();
                    ProcessImage(image, tags);
                    
                    Image img = new Image(item.Name, item.Name.Split('.')[1], tags, image.Height, image.Width, file.Length);
                    imagesList.Add(img);
                    listOfImages.Items.Add(item);
                }
            }
            catch
            {
                return;
            }
            finally
            {
                //обновляем статистику
                lblLoaded.Text = String.Format("Загружено изображений: {0}", imagesCount);
                lblCircles.Text = String.Format("Кругов: {0}", Circle);
                lblTriangles.Text = String.Format("Треугольников: {0}", Triangle);
                lblQuadrilaterals.Text = String.Format("Четырехугольников: {0}", Quadrilateral);
            }
        }

        // Process image
        private void ProcessImage(Bitmap bitmap, List<string> tags)
        {
            // lock image
            BitmapData bitmapData = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, bitmap.PixelFormat);

            // step 1 - turn background to black
            ColorFiltering colorFilter = new ColorFiltering();

            colorFilter.Red = new IntRange(0, 64);
            colorFilter.Green = new IntRange(0, 64);
            colorFilter.Blue = new IntRange(0, 64);
            colorFilter.FillOutsideRange = false;

            colorFilter.ApplyInPlace(bitmapData);

            // step 2 - locating objects
            BlobCounter blobCounter = new BlobCounter();

            blobCounter.FilterBlobs = true;
            blobCounter.MinHeight = 5;
            blobCounter.MinWidth = 5;

            blobCounter.ProcessImage(bitmapData);
            Blob[] blobs = blobCounter.GetObjectsInformation();
            bitmap.UnlockBits(bitmapData);

            // step 3 - check objects' type
            SimpleShapeChecker shapeChecker = new SimpleShapeChecker();
            for (int i = 0, n = blobs.Length; i < n; i++)
            {
                List<IntPoint> edgePoints = blobCounter.GetBlobsEdgePoints(blobs[i]);

                Accord.Point center;
                float radius;

                // is circle ?
                if (shapeChecker.IsCircle(edgePoints, out center, out radius))
                {
                    tags.Add("круг");
                    Circle++;
                }
                else
                {
                    List<IntPoint> corners;

                    // is triangle or quadrilateral
                    if (shapeChecker.IsConvexPolygon(edgePoints, out corners))
                    {
                        // get sub-type
                        PolygonSubType subType = shapeChecker.CheckPolygonSubType(corners);

                        if (subType == PolygonSubType.Unknown)
                        {
                            if (corners.Count == 4)
                            {
                                tags.Add("четырехугольник");
                                Quadrilateral++;
                            }
                        }
                        else if (subType == PolygonSubType.Trapezoid)
                        {
                            tags.Add("трапеция");
                            tags.Add("четырехугольник");
                            Trapezoid++;
                            Quadrilateral++;
                        }
                        else if (subType == PolygonSubType.Parallelogram)
                        {
                            tags.Add("параллелограмм");
                            tags.Add("четырехугольник");
                            Parallelogram++;
                            Quadrilateral++;
                        }
                        else if (subType == PolygonSubType.Rectangle)
                        {
                            tags.Add("прямоугольник");
                            tags.Add("четырехугольник");
                            Rectangle++;
                            Quadrilateral++;
                        }
                        else if (subType == PolygonSubType.Rhombus)
                        {
                            tags.Add("ромб");
                            tags.Add("четырехугольник");
                            Rhombus++;
                            Quadrilateral++;
                        }
                        else if (subType == PolygonSubType.Square)
                        {
                            tags.Add("квадрат");
                            tags.Add("четырехугольник");
                            Square++;
                            Quadrilateral++;
                        }
                        else if (subType == PolygonSubType.EquilateralTriangle)
                        {
                            tags.Add("равносторонний");
                            tags.Add("треугольник");
                            Triangle++;
                            EquilateralTriangle++;
                        }
                        else if (subType == PolygonSubType.IsoscelesTriangle)
                        {
                            tags.Add("равнобедренный");
                            tags.Add("треугольник");
                            Triangle++;
                            IsoscelesTriangle++;
                        }
                        else if (subType == PolygonSubType.RectangledTriangle || subType == PolygonSubType.RectangledIsoscelesTriangle)
                        {
                            tags.Add("прямоугольный");
                            tags.Add("треугольник");
                            Triangle++;
                            RectangledTriangle++;
                            RectangledIsoscelesTriangle++;
                        }
                    }
                }
            }
        }



        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearchBox.Text == "")
            {
                OnLoad(e);
            }
            else
            {
                listOfImages.Clear();
                //разбиваем строку на теги
                string[] tags = txtSearchBox.Text.Split(new[] { ' ', ','}, StringSplitOptions.RemoveEmptyEntries);

                List<string> currectImages = new List<string>();

                //пробный ДОЛГИЙ поиск
                foreach (var image in imagesList)
                {
                    for (int i = 0; i < tags.Length; i++)
                    {
                        if (image.getTags().Contains(tags[i].ToLower()) && !currectImages.Contains(image.name))
                        {
                            // путь до папки директории
                            var path = new DirectoryInfo(Path.Combine(Application.StartupPath, "Resources\\"));

                            ImageList imageList = new ImageList();
                            Bitmap img = (Bitmap)Bitmap.FromFile(path + image.name);

                            string shortName = image.name;
                            string imageKey = path + image.name;

                            imageList.Images.Add(imageKey, img);

                            ListViewItem item = new ListViewItem();
                            item.ImageKey = imageKey;
                            item.Name = shortName;
                            item.Text = shortName;
                            listOfImages.Items.Add(item);

                            currectImages.Add(image.name);
                        }
                    }
                }
            }
        }

        private void btnAddImage_Click(object sender, EventArgs e)
        {
           
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    List<string> imagesNames = Directory.GetFiles(Path.GetFileName(Path.Combine(Application.StartupPath, "Resources"))).ToList<string>();
                    for (int i = 0; i < imagesNames.Count; i++)
                    {
                        imagesNames[i] = imagesNames[i].Split('\\')[1];
                    }
                    int counter = 0;
                    string fileName = Path.GetFileName(openFileDialog.FileName);
                    while (imagesNames.Contains(fileName))
                    {
                        fileName = String.Format("{0} ({1}){2}", Path.GetFileNameWithoutExtension(openFileDialog.FileName), counter, Path.GetExtension(openFileDialog.FileName));
                        counter++;
                    }
                    File.Copy(openFileDialog.FileName, Path.Combine(Application.StartupPath, "Resources", fileName));
                    txtSearchBox.Clear();
                    OnLoad(e);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }

        private void listOfImages_ItemActivate(object sender, EventArgs e)
        {
            ImageBox.Show(displayImages[listOfImages.SelectedItems[0].ImageKey]);
        }

        private void listOfImages_Click(object sender, EventArgs e)
        {
            //if (listOfImages.Select.off)
            foreach (var image in imagesList)
            {
                if (listOfImages.SelectedItems[0].Name == image.name)
                {
                    lblNamePic.Text = String.Format("Название: {0}", image.name.Split('.'));
                    lblExtension.Text = String.Format("Расширение: {0}", image.extension);
                    lblHWstat.Text = String.Format("Размер: {0} x {1}", image.height, image.width);
                    lblSize.Text = String.Format("Вес: {0} байт", image.size);
                    break;
                }
            }
        }

        public static IEnumerable<FileInfo> GetFilesByExtensions(DirectoryInfo dir, params string[] extensions)
        {
            if (extensions == null)
            {
                throw new ArgumentNullException("extensions");
            }
            IEnumerable<FileInfo> files = dir.EnumerateFiles();
            return files.Where(f => extensions.Contains(f.Extension));
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtSearchBox.Clear();
            OnLoad(e);
        }
    }
}
